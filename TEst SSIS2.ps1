#$global:DEPLOY_PATH = $OctopusParameters["DM_DEPLOY_PATH"]
#$global:DB_HOST = $OctopusParameters["DB_HOST"]
#$global:DB_USER = $OctopusParameters["DB_USER"]
#$global:DB_PASS = $OctopusParameters["DB_PASS"]
#$global:EnvironmentName = $OctopusParameters["EnvironmentName"]
#$global:ETL_Deploy_Path = $OctopusParameters["ETL_Deploy_Path"]


$DB_HOST = "UE2POC01DATA08"
$DB_USER = "POC\amit.kumar"
$DB_PASS = "orange#5"

$FolderName = $args[0]
$ETL_Deploy_Path = "F:\ETL_Package"

# Provide the location of the Project file with path

if ( $FolderName -eq "CCB.CALC" )
{
Write-Host $FolderName
$ProjectFilePath = "$($ETL_Deploy_Path)\CCB.CALC\bin\Development\CCB.ispac"
} 
elseif ( $FolderName -eq "CCB.ETL" )
{
Write-Host $FolderName
$ProjectFilePath = "$($ETL_Deploy_Path)\CCB.ETL\CCB.ETL\CCB.ETL\bin\Development\CCB.ispac"
}
elseif ( $FolderName -eq "CCB.ETL.ROLLING" )
{
$ProjectFilePath = "$($ETL_Deploy_Path)\CCB.ETL\CCB.ETL.ROLLING\OSI.SHELTER.CCB.ETL.ROLLING\bin\Development\OSI.SHELTER.CCB.ETL.ispac"
}
elseif ( $FolderName -eq "COMMISSION_CONSOLIDATED_PKG" ) 
{
$ProjectFilePath = "$($ETL_Deploy_Path)\COMMISSION_CONSOLIDATED_PKG\COMMISSION_CONSOLIDATED_PKG\bin\Development\COMMISSION_CONSOLIDATED_PKG.ispac"
} 
else 
{
    Write-Host "Please pass correct Value."
}

Write-Host "Going to Deploy $ProjectFilePath"

$ProjectName = $FolderName

# Provide the SQL Connection details and Folder names

#$sqlConnectionString = "Data Source=$($DB_HOST);Initial Catalog=master;Integrated Security=false;User ID=$($DB_USER);Password=$($DB_PASS);"

$sqlConnectionString = "Data Source=$($DB_HOST);Initial Catalog=master;Integrated Security=SSPI;"

#$FolderName = "CCB.ETL"
$FolderDescription = $FolderName
 
# Load the IntegrationServices Assembly

[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Management.IntegrationServices") | Out-Null;
 
# Create a connection object based on the connection string
# This connection will be used to connect to the Integration services service

$sqlConnection = New-Object System.Data.SqlClient.SqlConnection $sqlConnectionString
 
# Let's create a new Integration Services object based on the SSIS name space and 
# the connection object created in the previous step

$integrationServices = New-Object Microsoft.SqlServer.Management.IntegrationServices.IntegrationServices $sqlConnection



write-Host $integrationServices.Catalogs["SSISDB"]
 
$catalog = $integrationServices.Catalogs["SSISDB"]
 
#As we have created an object for the catalog, we can access the catalog
# and create a folder

Write-Host "Creating Folder " $FolderName "  in the SSIS Catalog"
 
$SSISfolder = New-Object Microsoft.SqlServer.Management.IntegrationServices.CatalogFolder ($catalog, $FolderName, $FolderDescription)
$SSISfolder.Create()
Write-Host $FolderName "Folder has been created in the SSIS Catalog"
 
Write-Host "Deploying " $ProjectName " project ..."

# Read the project file, and deploy it to the folder
[byte[]] $projectFile = [System.IO.File]::ReadAllBytes($ProjectFilePath)
$SSISfolder.DeployProject($ProjectName, $projectFile)
Write-Host $ProjectName " project has been deployed successfully"